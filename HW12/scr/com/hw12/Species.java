package com.hw12;

public enum Species {
    CAT,
    DOG,
    PARROT,
    DOMESTICCAT,
    FISH,
    ROBOCAT,
    UNKNOWN
}
