package main.java.main;

public enum Species {
    CAT,
    DOG,
    PARROT,
    DOMESTICCAT,
    FISH,
    ROBOCAT,
    UNKNOWN
}
