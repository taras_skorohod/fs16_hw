package ua.com.hw3;

import java.util.Arrays;
import java.util.Scanner;

public class planner {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.println("Welcome");
    String n = "Please enter name: ";
    for (int i = 0; i < 1; i++) {
      try{
        Thread.sleep(400);
      }catch (InterruptedException e ){
        e.printStackTrace();
      }
      System.out.print(n);
    }
    String name = scanner.next();

    System.out.println(Arrays.toString(new String[]{name}) + " Welcome to your Diaries)");
    String userCommand;

    String [][] schedule = new String[7][2];

    schedule[0][0] = "Sunday";
    schedule[0][1] = "do home work";

    schedule[1][0] = "Monday";
    schedule[1][1] = "go to courses; watch a film";

    schedule[2][0] = "Tuesday";
    schedule[2][1] = "buy a TV";

    schedule[3][0] = "Wednesday";
    schedule[3][1] = "work";

    schedule[4][0] = "Thursday";
    schedule[4][1] = "to work a lot";

    schedule[5][0] = "Friday";
    schedule[5][1] = "read a book";

    schedule[6][0] = "Saturday";
    schedule[6][1] = "go to museum;";

    do {
      System.out.print(Arrays.toString(new String[]{name}) + " Please, input the day of the week: ");
      userCommand = scanner.next().trim();
      boolean isNotCorrect = true;
      boolean isChange = false;
      String [] splitCommand = userCommand.split(" ");

      if (checkWordsWithIgnoreCase(userCommand, "exit")){
        break;
      }
      if (splitCommand.length > 1 && checkWordsWithIgnoreCase(splitCommand[0], "change")){
        userCommand = splitCommand[1];
        isChange = true;
      }
      for (int i = 0; i < schedule.length; i++){
        if (checkWordsWithIgnoreCase(userCommand, schedule[i][0])){
          if (isChange){
            System.out.printf("What tasks will you have on %s:\n", schedule[i][0]);
            String userCommandTasks = scanner.next().trim();
            schedule[i][0] = userCommandTasks;
          }else {
            System.out.printf("Your tasks for %s: %s\n",schedule[i][0], schedule[i][1]);
          }
          isNotCorrect =false;
          }
        }if (isNotCorrect){
        System.out.println("Sorry, I don't understand you, please try again.");
      }
    }while (true);
  }

  public static boolean checkWordsWithIgnoreCase(String firstWord,String secondWord){
    return firstWord.equalsIgnoreCase(secondWord);
  }
}
