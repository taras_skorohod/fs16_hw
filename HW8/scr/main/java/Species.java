package main.java;

public enum Species {
    CAT,
    DOG,
    PARROT,
    DOMESTICCAT,
    FISH,
    ROBOCAT,
    UNKNOWN
}
