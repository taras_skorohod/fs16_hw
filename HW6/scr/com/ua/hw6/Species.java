package com.ua.hw6;

public enum Species {
  CAT,
  DOG,
  PARROT,
  DOMESTICCAT,
  FISH,
  ROBOCAT,
  UNKNOWN
}
