package com.hw10;

import java.text.ParseException;
import java.util.HashSet;
import java.util.Set;

public class Main {
  public static void main(String[] args) throws ParseException {
    FamilyController f = new FamilyController();
    Man m1 = new Man("test1","test1",25);
    Woman w1 = new Woman("test1","test1",22);
    Man m2 = new Man("test2","test2",33);
    Woman w2 = new Woman("test2","test2",22);

    Family f1 = new Family(m1, w1);
    Family f2 = new Family(m2, w2);

    f.saveFamily(f1);
    f.saveFamily(f2);

    f.deleteFamily(0);
    f.saveFamily(f2);

    f.saveFamily(f1);
    f.createNewFamily(w1,m2);

    f.bornChild(f.getAllFamilies().get(1),"Joha","Anny");

    Woman c1 = new Woman("Andree", "Skor", 22);
    f.adoptChild(f.getAllFamilies().get(1), c1);
    f.deleteAllChildrenOlderThen(18);
    f.count();
    Set<String> habbits = new HashSet<String>();
    Pet p1 = new DomesticCat("bob",2,(byte) 55,habbits);
    f.addPet(1,p1);
    f.getPets(1);


    f.displayAllFamilies();
    System.out.println(f.getFamilyByIndex(0));


    System.out.println(f.getFamiliesBiggerThan(3));
    System.out.println(f.getFamiliesLessThan(3));
    System.out.println(f.countFamiliesWithMemberNumber(1));
    System.out.println(f.countFamiliesWithMemberNumber(3));

  }
}
