package com.hw9;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Main {
  public static void main(String[] args) throws ParseException {


    String date1 = "02/11/1996";
    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

    Date dateOne = format.parse(date1);
    Man m1 = new Man("Taras", "Skorohod", "02/11/1996", (byte) 100);
    System.out.println(m1.describeAge());
    System.out.println(m1.toString());
  }
}
