package com.hw11;

public class FamilyOverflowException extends RuntimeException  {

  public FamilyOverflowException(){

  }
  public FamilyOverflowException(String massage){
    super(massage);
  }
  public FamilyOverflowException( String massage, Throwable cause){

    super(massage, cause);
  }
  public FamilyOverflowException(Throwable cause){
    super(cause);
  }
  public FamilyOverflowException(String massage, Throwable cause, boolean enableSuppression, boolean writableStackTrace){
    super(massage, cause, enableSuppression, writableStackTrace);
  }



}
